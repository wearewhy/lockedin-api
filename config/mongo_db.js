var mongoose            = require('mongoose');
var bcrypt              = require('bcrypt');
var SALT_WORK_FACTOR    = 10;
var mongodbURL          = process.env.MONGOLAB_URI || process.env.MONGOHQ_URL || 'mongodb://localhost/lockedin';
var mongodbOptions      = { };

mongoose.connect(mongodbURL, mongodbOptions, function (err, res) {
    if (err) { 
        console.log('Connection refused to ' + mongodbURL);
        console.log(err);
    } else {
        console.log('...Mongo is ready');
    }
});

var Schema = mongoose.Schema;

// User schema
var User = new Schema({
    access_token    : String,
    online          : { type: Boolean, default: true },
    advice_accepted : { type: Boolean, default: false },
    shoutouts_r     : { type: [String], 'default': [] },
    shoutouts_s     : { type: [String], 'default': [] },
    facebook        : Object,
    twitter         : Object,
    google          : Object,
    instagram       : Object,
    bbcplaylister   : Object,
    preferences     : {
        email           : String,
        password        : { type: String},
        name            : String,
        initials        : String,
        image           : String,
        description     : String,
        is_dj           : { type: Boolean,'default': false },        
        latitude        : { type: String, 'default': ''},
        longitude       : { type: String, 'default': ''},
        socketioID      : { type: String, 'default': '' }
    },
    creationDate    : { type: Date, 'default':Date.now }
});

User.pre('save', function(next) {
    var user = this;

    if (!user.preferences.password) return next();

    // only hash the password if it has been modified (or is new)
    if (!user.preferences.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password along with our new salt
        bcrypt.hash(user.preferences.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.preferences.password = hash;
            next();
        });
    });
});

User.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

//Define Models
var userModel = mongoose.model('User', User);

// Export Models
exports.userModel = userModel;