var express           = require('express');
var app               = module.exports = express();
var port              = process.env.PORT || 3000;
var jwt               = require('express-jwt');
var bodyParser        = require('body-parser');
var morgan            = require('morgan');
var tokenManager      = require('./config/token_manager');
var secret            = require('./config/secret');
var http              = require('http');
var server            = http.createServer(app);
// forms
process.env.PWD       = process.cwd();
var busboy            = require('connect-busboy');
var path              = require('path');
var crypto            = require('crypto');

var allowCrossDomain  = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

    if ('OPTIONS' == req.method) {
      res.send(200);
    } else {
      next();
    }
};

if ('development' == app.get('env')) {
  app.use(allowCrossDomain);
  app.use(bodyParser());
  app.use(morgan());
  app.use(busboy());
  app.use(express.static(__dirname + '/public')); //Declaring a public folder to let the users get access to it
}

var AWS_ACCESS_KEY  = process.env.AWS_ACCESS_KEY_ID;
var AWS_SECRET_KEY  = process.env.AWS_SECRET_ACCESS_KEY;
var S3_BUCKET       = process.env.S3_BUCKET_NAME;

//app.listen(port);
var routes      = {};
routes.users    = require('./route/users.js');

// SOCKET.IO
server.listen(port);
require('./config/socket-io')(app, server, secret);

// CALLS
app.post('/user', routes.users.register);                                                                           // Creates a new user
app.put('/user', routes.users.update);                                                                              // Update the user
app.delete('/user', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.users.remove);              // Remove that user from the db
app.get('/user', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.users.status);                 // Returns the information about the current user

app.post('/user/bbcplaylister', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.users.bbc_playlister);

app.put('/user/online', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.users.onlineUpdate);     // Will put the user as online / offline
app.get('/user/preferences', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.users.preferences); // Returns the information about the current user
app.post('/user/reftoken', routes.users.refresh_token);                                                               // Returns the information about the current user

app.post('/user/merge', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.users.merge);           // Marge accounts with current account
app.post('/user/logout', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.users.logout);         // Logout the user

app.get('/users/all/:numpage', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.users.listAll);  // Return the users connected separated by pages
app.get('/users/number', routes.users.listAllNumber);                                                               // Return the number of users connected

app.post('/shoutout', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.users.send_shoutout);  
app.get('/shoutout', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.users.shoutouts);  

/******* DJ CALLS ********/
app.post('/dj/register', routes.users.dj_register);
app.post('/dj/login', routes.users.dj_login);
app.post('/dj/picture', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.users.dj_picture);
app.post('/dj/:id', jwt({secret: secret.secretToken}), tokenManager.verifyToken, routes.users.dj_details);

/*
app.get('/sign_s3', function(req, res){
  var object_name = req.query.s3_object_name;
  var mime_type   = req.query.s3_object_type;
  AWS_QUERYSTRING_EXPIRE = 63115200;
  var now = new Date();
  var expires = Math.ceil((now.getTime() + 10000)/1000); // 10 seconds from now
  var amz_headers = "x-amz-acl:public-read";

    var put_request = "PUT\n\n"+mime_type+"\n"+expires+"\n"+amz_headers+"\n/"+S3_BUCKET+"/"+object_name;

    var signature = crypto.createHmac('sha1', AWS_SECRET_KEY).update(put_request).digest('base64');
    signature = encodeURIComponent(signature.trim());
    signature = signature.replace('%2B','+');

    var url = 'https://'+S3_BUCKET+'.s3.amazonaws.com/'+object_name;

    var credentials = {
        signed_request: url+"?AWSAccessKeyId="+AWS_ACCESS_KEY+"&Expires="+expires+"&Signature="+signature,
        url: url
    };
    res.write(JSON.stringify(credentials));
    res.end();
});*/