#POST dj/register

Will create a DJ user. The call will return a token and the DJ user will be logged in as well.

**Resource URL**
/dj/register

##Parameters
The parameters can come from whatever that generates a POST call, and the variables has to be named as the API requires:

```javascript
- username (email)
- password
- name
- initials
```


##Headers

*No need headers*

##Example

```html
<form id="registerForm" action="dj/register" method="post">
    <input type="text" name="username" placeholder="email"/>
   <input type="password" name="password"  placeholder="pass"/>
   <input type="text" name="name" placeholder="name"/>
   <input type="text" name="initials" placeholder="initials"/>
   <input type="submit" value="Send" />
</form>
```
```javascript
$("#registerForm").submit(function(e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        data: $("#registerForm").serialize(),
        url: root + $(this).attr("action")
    }).done(function(msg) {
        token = msg.token;
        console.log("token : "+token);
    });
});
```

#GET dj/login

This call will login any DJ user. The API will look for the information to the mongodb and will return the token. 

**Resource URL**
/dj/login

##Parameters
*No parameters*


##Headers
*No need headers*

##Example
```html
<form id="loginForm" action="dj/login" method="post">
    <input type="text" name="username" placeholder="email"/><br/>
    <input type="password" name="password"  placeholder="pass"/><br/>
    <input type="submit" value="Send" />
</form>
```

```javascript
$("#loginForm").submit(function(e) {
    e.preventDefault();
    $.ajax({
        type: "POST",
        data: $("#loginForm").serialize(),
        url: root + $(this).attr("action")
    }).done(function(msg) {
        token = msg.token;
        console.log("token : "+token);
    });
});
```