# Welcome


## [User](https://bitbucket.org/kentlyons/lockedin-api/wiki/wiki/User)

All the calls and the information related about the /user stuff. Calls to register, to login, get the information about the user, logout and delete. Know the parameters that be need to pass in each call and see an examples with $.ajax jquery call.

## [Users](https://bitbucket.org/kentlyons/lockedin-api/wiki/wiki/Users)

All the information about the calls related to many users. For example, to see all the users connected at this moment.

## [Shout outs](https://bitbucket.org/kentlyons/lockedin-api/wiki/wiki/Shoutouts)

All the calls related to the Shout out. To make a "shout out" and to know who "Shouted out" the user logged in.

## [Sockets - socket.io](https://bitbucket.org/kentlyons/lockedin-api/wiki/wiki/Sockets)

How to work with sockets. It is useful when we use Shout outs in real-time and we want that the receiver of the message is notified.

#Setup repositories & ssh connections 

##HEROKU repositories

###API

Add the remote of the repository: 
````
$ git remote add heroku_api git@heroku.com:lockedinapi.git
````
##BBC repositories

###API

Add the remote of the repository: 
````
$ git remote add bbc_api lockedin@10.0.1.167:~/repositories/lockedin_api.git
````

###Frontend

Add the remote of the repository: 
````
$ git remote add bbc_frontend lockedin@10.0.1.167:~/repositories/lockedin_app.git
````


##SSH Connections

This file will help to connect via SSH to the BBC servers. We'll create two ssh connections:

- 10.0.1.167
- bastion.pilots.bbconnectedstudios.co.uk

###1. Create a ssh config file


````
$ vim ~/.ssh/config
````

###2. Writing some content

````bash
Host bbc_vm
    HostName 10.0.1.167
    User locked
    ProxyCommand /usr/bin/ssh -q -T bastion nc %h %p

Host bastion
    HostName bastion.pilots.bbcconnectedstudio.co.uk
    User lockedin
````

###3. How to invoke

When we need to connecto via ssh we have to write *$ ssh Host*:

````
$ ssh bbc_vm
````
or
````
$ ssh bastion
````

Everything is installed in **bbc_vm** (repositories, website folder, etc.).